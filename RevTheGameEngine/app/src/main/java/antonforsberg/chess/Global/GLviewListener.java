package antonforsberg.chess.Global;

/**
 * Created by Anton Forsberg on 2017-12-19.
 */

public interface GLviewListener {
    void viewUpdate();
}
