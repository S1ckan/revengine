package antonforsberg.chess.comobject.objectAssets;

import antonforsberg.chess.comobject.objectAssets.ObjectInterface.BasicObject;

/**
 * Created by Anton Forsberg on 2017-12-05.
 */

public class StandardObject implements BasicObject {

    @Override
    public void draw(float[] mMVPMatrix, float[] mProjectionMatrix, float[] mViewMatrix, float[] mModelMatrix) {

    }

    @Override
    public void loadAssets() {

    }
}
