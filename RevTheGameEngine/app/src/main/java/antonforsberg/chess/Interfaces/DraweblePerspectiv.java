package antonforsberg.chess.Interfaces;

/**
 * Created by Anton Forsberg on 2017-12-16.
 */

public interface DraweblePerspectiv {

    void draw(float [] mMVPMatrix,float [] mProjectionMatrix,float [] mViewMatrix,float [] mModelMatrix);
}
