package antonforsberg.chess.UserInput;

/**
 * Created by Anton Forsberg on 2017-12-16.
 */

public interface UiPressListener {
    void press(float x ,float y);
}
