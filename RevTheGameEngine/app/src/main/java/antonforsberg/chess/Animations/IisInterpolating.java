package antonforsberg.chess.Animations;

/**
 * Created by Anton Forsberg on 2017-12-18.
 */

public interface IisInterpolating {
    boolean isInterpolating();
}
