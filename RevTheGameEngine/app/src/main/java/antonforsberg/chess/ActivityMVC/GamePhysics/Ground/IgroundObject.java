package antonforsberg.chess.ActivityMVC.GamePhysics.Ground;

import android.graphics.Point;

public interface IgroundObject {
    Point get();
}
